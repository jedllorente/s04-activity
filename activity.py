from abc import ABC, abstractclassmethod
# Abstract Class


class Abstract_Animal_Methods(ABC):
    @abstractclassmethod
    def eat(self):
        pass

    @abstractclassmethod
    def make_sound(self):
        pass

    @abstractclassmethod
    def call(self):
        pass


class Animal(Abstract_Animal_Methods):
    def __init__(self, name, breed, age):
        self._name = name
        self._breed = breed
        self._age = age

    def get_name(self):
        return self._name

    def get_breed(self):
        return self._breed

    def get_age(self):
        return self._age

    def set_name(self, name):
        self._name = name

    def set_breed(self, breed):
        self._name = breed

    def set_age(self, age):
        self._name = age


class Cat(Animal):
    def eat(self, food):
        print(f"Ate {food}")

    def make_sound(self, breed):
        print("Nyan, Meow, Mao")

    def call(self):
        print(f"{self.name}, come!")


class Dog(Animal):
    def eat(self, food):
        print(f"Ate {food}")

    def make_sound(self):
        print("Bark! Awooo~ Woof!")

    def call(self):
        print(f"Come {self.name}!")


cat1 = Cat("Creamo", "Puspin", 18)
cat1.eat("Chicken")
cat1.make_sound()
cat1.call()

dog1 = Dog("Duke", "Belgian Mallinois", 17)
dog1.eat("Beef")
dog1.make_sound()
dog1.call()
